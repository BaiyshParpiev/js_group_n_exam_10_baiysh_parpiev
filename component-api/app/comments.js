const express = require('express');
const mysqlDb = require("../mysqlDb");

const router = express.Router();


router.get('/:id', async (req, res) => {
    const [comments] = await mysqlDb.getConnection().query('select * from ?? where posts_id = ?;', ['comments', req.params.id]);
    if(!comments.author){
        comments.author = 'Anonymous'
    }
    res.send(comments);
});


router.post('/', async (req, res) => {
    try {
        if (!req.body.comment) {
            return res.status(404).send({error: 'Date not valid'});
        }

        const comment = {
            posts_id: req.body.posts_id,
            author: req.body.author,
            comment: req.body.comment,
        };

        const [newComment] = await mysqlDb.getConnection().query(
            'INSERT INTO ?? (posts_id, author, comment) values(?, ?, ?)',
            ['comments', comment.posts_id, comment.author, comment.comment]
        );

        res.send({
            ...comment,
            id: newComment.insertId
        });
    } catch (e) {
        console.log(e)
    }
});


router.delete('/:id', async (req, res) => {
    try {
        await mysqlDb.getConnection().query(
            'Delete from ?? where id = ?',
            ['comments', req.params.id]
        )
        res.send('Deleted successful!')
    } catch (e) {
        res.status(401).send(e.sqlMessage)
    }
});

module.exports = router
