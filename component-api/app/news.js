const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid')
const config = require('../config');
const mysqlDb = require("../mysqlDb");
// const fileDb = require('../fileDb');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});


const upload = multer({storage});
const router = express.Router();



router.get('/', async(req, res) => {
  // const products = fileDb.getItems(); when we want to create api in this directory after using DBSM we will not use it;
  const [products] = await mysqlDb.getConnection().query('SELECT * from ??', ['posts']);
  res.send(products);
});

router.get('/:id', async(req, res) => {
  // const product = fileDb.getItem(req.params.id); //file in this directory
  const [product] = await mysqlDb.getConnection().query(
    `SELECT * from ?? where id = ? `, ['posts',req.params.id]
  );
  if(!product){
    return res.status(404).send({error: 'Product not Found'});
  }
  res.send(product[0]);
});

router.post('/', upload.single('image'), async(req, res) => {
  if(!req.body.title || !req.body.description){
    return res.status(404).send({error: 'Date not valid'});
  }

  const product = {
    title: req.body.title,
    description: req.body.description,
  }

  if(req.file){
    product.image = req.file.filename;
  }

  // const newProduct = fileDb.addItem(product) file db
  const [newProduct] = await mysqlDb.getConnection().query(
    'INSERT INTO ?? (title,  description, image) values(?, ?, ?)',
    ['posts', product.title,  product.description, product.image]
  );

  res.send({
    ...product,
    id: newProduct.insertId
  });
});

router.put('/:id', upload.single('image'), async(req, res) => {
  const product = {
    title: req.body.title,
    description: req.body.description,
  };

  if(req.file) product.image = req.file.filename;

  await mysqlDb.getConnection().query(
    'UPDATE ?? set ? where id = ?',
    ['posts', {...product}, req.params.id]
  );

  res.send({message: `Update successful, id=${req.params.id}`});
})

module.exports = router
