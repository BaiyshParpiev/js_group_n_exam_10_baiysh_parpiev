import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Grid, Button, CardMedia, Paper, Typography} from "@material-ui/core";
import {getNewsItem} from '../../store/actions/newsActions';
import imageNotAvailable from "../../assets/images/not_available.png";
import {makeStyles} from "@material-ui/core/styles";
import {deleteComments, getComments} from "../../store/actions/commentActions";
import {createComments} from "../../store/actions/commentActions";
import CommentsForm from '../../components/CommentsForm/CommentsForm';

const useStyles = makeStyles(theme => ({
    media: {
        height: 0,
        paddingTop: '56.25%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        backgroundBlendMode: 'darken',
    },
}));


const Post = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {newsItem} = useSelector(state => state.news)
    const {comments} = useSelector(state => state.comments)

    useEffect(() => {
        dispatch(getNewsItem(match.params.id));
    }, [dispatch, match.params.id])

    const createComment = (com) => {
        dispatch(createComments(com))
    }
    const onCommentHandler = id => {
        dispatch(getComments(id))
    };

    const onRemoveHandler =id => {
        dispatch(deleteComments(id))
    }
    return newsItem && (
        <Paper component={Box} p={2}>
            <Typography variant='h4'>
                {newsItem.title}
            </Typography>
          <Typography variant='body1'>
                {newsItem.description}
            </Typography>
            <CardMedia className={classes.media} image={newsItem.image ? 'http://localhost:8000/uploads/' + newsItem.image : imageNotAvailable}/>
            <Typography variant='h4'>
                {newsItem.datetime}
            </Typography>
            <Button size="small" color="primary" onClick={() => onCommentHandler(newsItem.id)}>
                Comment
            </Button>
            {comments && comments.map(c => (
                        <Grid key={c.id} container flexdirection="row" justifyContent="space-between" alignItems="center"><Typography variant='h4'>
                        {c.author}
                    </Typography>
                    <Typography variant='body1'>
                {c.comment}
                    </Typography>
                    <Button size="small" color="primary" onClick={() => onRemoveHandler(match.params.id)}>
                    Delete
                    </Button>
                        </Grid>
                ))
            }
            <CommentsForm onSubmit={createComment}/>
        </Paper>
    );
};

export default Post;