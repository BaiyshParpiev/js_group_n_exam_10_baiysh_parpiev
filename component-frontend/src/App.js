import React from 'react';
import {Container, AppBar, Typography, Grow} from '@material-ui/core';
import memories from './assets/images/news.jpeg';
import useStyles from './styles';
import Posts from "./containers/Posts/Posts";
import NewPost from './containers/NewPost/NewPost';
import {Route, Switch} from "react-router-dom";
import Post from "./containers/Post/Post";

const App = () => {
  const classes = useStyles();
  return (
      <Switch>
          <Container maxWidth="lg">
              <AppBar className={classes.appBar} position="static" color="inherit">
                  <Typography className={classes.heading} variant="h2" align="center">News</Typography>
                  <img className={classes.image} src={memories} alt="memories" height="60"/>
              </AppBar>
              <Grow in>
                  <Container>
                      <Route path="/" exact component={Posts}/>
                      <Route path="/form/new" component={NewPost}/>
                      <Route path="/news/:id" component={Post}/>
                  </Container>
              </Grow>
          </Container>
      </Switch>
  );
};

export default App;